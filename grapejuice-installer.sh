#!/bin/bash

echo """ 
 ██████╗ ██████╗  █████╗ ██████╗ ███████╗     ██╗██╗   ██╗██╗ ██████╗███████╗ 
██╔════╝ ██╔══██╗██╔══██╗██╔══██╗██╔════╝     ██║██║   ██║██║██╔════╝██╔════╝
██║  ███╗██████╔╝███████║██████╔╝█████╗       ██║██║   ██║██║██║     █████╗  
██║   ██║██╔══██╗██╔══██║██╔═══╝ ██╔══╝  ██   ██║██║   ██║██║██║     ██╔══╝  
╚██████╔╝██║  ██║██║  ██║██║     ███████╗╚█████╔╝╚██████╔╝██║╚██████╗███████╗
 ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝     ╚══════╝ ╚════╝  ╚═════╝ ╚═╝ ╚═════╝╚══════╝
 """
echo "Welcome To The Grapejuice Installer!"
echo "Thins Installer was made for an easy install of grapejuice for playing roblox on linux"
echo "Please ensure that you have wine with version 6.11 or higher for roblox player"
echo "Press enter to continue."
read -r REPLY
echo "Checking linux distro"
if [ -f /etc/debian_version ]; then
    sudo apt install -y git python3-pip python3-setuptools python3-wheel python3-dev pkg-config libcairo2-dev gtk-update-icon-cache desktop-file-utils xdg-utils libgirepository1.0-dev gir1.2-gtk-3.0 git
    git clone https://gitlab.com/brinkervii/grapejuice.git /tmp/grapejuice
    cd /tmp/grapejuice
    python3 ./install.py
elif [ -f /etc/arch-release ]; then
    sudo sed -i "/\[multilib\]/,/Include/"'s/^#//' /etc/pacman.conf
    sudo pacman -S wine base-devel
    sudo pacman -S --asdep lib32-gnutls lib32-openssl lib32-pipewire lib32-libpulse lib32-alsa-lib lib32-alsa-plugins
    sudo pacman -S winetricks wine-mono wine-gecko zenity
    git clone https://gitlab.com/brinkervii/grapejuice.git /tmp/grapejuice
    cd /tmp/grapejuice
    ./install.py
    sudo pacman -S expac
    sudo pacman -S $(expac '%n %o' | grep ^wine)
else
    echo "The Installer only supports debian linux, arch linux and derivates at the moment!"
    echo "For other distros please go to the wiki: https://gitlab.com/brinkervii/grapejuice/-/wikis/home"
fi
